<?php
/**
 * @file
 * ns_ch_int.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ns_ch_int_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-ns-ch-int-user-menu:<front>
  $menu_links['menu-ns-ch-int-user-menu:<front>'] = array(
    'menu_name' => 'menu-ns-ch-int-user-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Help',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '1',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: menu-ns-ch-int-user-menu:user
  $menu_links['menu-ns-ch-int-user-menu:user'] = array(
    'menu_name' => 'menu-ns-ch-int-user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'Profile',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: menu-ns-ch-int-user-menu:user/logout
  $menu_links['menu-ns-ch-int-user-menu:user/logout'] = array(
    'menu_name' => 'menu-ns-ch-int-user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-47',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Help');
  t('Log out');
  t('Profile');


  return $menu_links;
}
