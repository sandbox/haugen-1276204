<?php
/**
 * @file
 * ns_ch_int.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_ch_int_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "dynamic_formatters" && $api == "dynamic_formatters_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "panels_mini" && $api == "panels_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ns_ch_int_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function ns_ch_int_node_info() {
  $items = array(
    'ns_ch_int_group' => array(
      'name' => t('Group'),
      'base' => 'node_content',
      'description' => t('A group that will have members and content associated with.'),
      'has_title' => '1',
      'title_label' => t('Group name'),
      'help' => '',
    ),
  );
  return $items;
}
