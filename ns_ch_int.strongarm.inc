<?php
/**
 * @file
 * ns_ch_int.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ns_ch_int_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_ns_ch_int_group';
  $strongarm->value = '1';
  $export['language_content_type_ns_ch_int_group'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_ns_ch_int_group';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_ns_ch_int_group'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_ns_ch_int_group';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_ns_ch_int_group'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_ns_ch_int_group';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_ns_ch_int_group'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_ns_ch_int_group';
  $strongarm->value = 0;
  $export['node_submitted_ns_ch_int_group'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'og_group_type_ns_article';
  $strongarm->value = 'omitted';
  $export['og_group_type_ns_article'] = $strongarm;

  return $export;
}
