<?php
/**
 * @file
 * ns_book.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ns_book_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ns_book_node_info() {
  $items = array(
    'ns_book_page' => array(
      'name' => t('Book page'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
