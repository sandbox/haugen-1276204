<?php
/**
 * @file
 * ns_ch_int.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function ns_ch_int_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-ns-ch-int-useful-links
  $menus['menu-ns-ch-int-useful-links'] = array(
    'menu_name' => 'menu-ns-ch-int-useful-links',
    'title' => 'Useful links',
    'description' => 'Links that are useful to your users.',
  );
  // Exported menu: menu-ns-ch-int-user-menu
  $menus['menu-ns-ch-int-user-menu'] = array(
    'menu_name' => 'menu-ns-ch-int-user-menu',
    'title' => 'Intranet user menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Intranet user menu');
  t('Links that are useful to your users.');
  t('Useful links');


  return $menus;
}
